package br.com.iosys.iosysrating.dto;

import br.com.iosys.iosysrating.model.Rating;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RatingDto {

    @Schema(name = "rating", description = "Rating movie between 1 and 4")
    @Min(value = 1)
    @Max(value = 4)
    private Integer rating;

    @Schema(name = "idMovie", description = "Unique movie identifier")
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Long idMovie;

    @Schema(name = "idUser", description = "Unique user identifier")
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Long idUser;

    public static RatingDto prepareRatingDto(Rating rating) {
        return RatingDto.builder()
                .idMovie(rating.getIdMovie())
                .idUser(rating.getIdUser())
                .rating(rating.getRating())
                .build();
    }

}
