package br.com.iosys.iosysrating.dto;

import br.com.iosys.iosysrating.model.Artist;
import br.com.iosys.iosysrating.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ArtistDto {

    private Long id;
    private String name;

    public static ArtistDto prepareArtist(Artist artist) {
        return ArtistDto
                .builder()
                .id(artist.getId())
                .name(artist.getName())
                .build();
    }

}
