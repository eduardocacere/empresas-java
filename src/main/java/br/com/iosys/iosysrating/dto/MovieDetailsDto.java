package br.com.iosys.iosysrating.dto;

import br.com.iosys.iosysrating.model.Movie;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MovieDetailsDto {

    private String name;

    private String description;

    private String director;

    private GenderDto gender;

    private LocalDate dateRelease;

    private List<ArtistDto> castMovies;

    private Integer votingMedia;

    public static MovieDetailsDto prepareDetails(MovieDto movie) {
        return MovieDetailsDto.builder()
                .name(movie.getName())
                .description(movie.getDescription())
                .director(movie.getDirector())
                .build();
    }
}
