package br.com.iosys.iosysrating.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Optional;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MovieDataDto {

    private Optional<String> nameMovie;
    private Optional<Long> idGender;
    private List<Long> idArtists;
}
