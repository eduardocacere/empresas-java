package br.com.iosys.iosysrating.dto;

import br.com.iosys.iosysrating.model.Gender;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GenderDto {

    @Schema(name = "id", description = "Unique gender identifier")
    private Long id;

    @Schema(name = "name", description = "Gender name identification")
    private String name;

    public static GenderDto prepareGenderDto(Gender gender) {
        return GenderDto.builder()
                .id(gender.getId())
                .name(gender.getName())
                .build();
    }
}
