package br.com.iosys.iosysrating.dto;

import br.com.iosys.iosysrating.model.User;
import br.com.iosys.iosysrating.model.enums.ProfileEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Profile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LoginSignupDto {

    @NotEmpty(message = "javax.validation.constraints.NotEmpty.message")
    private String email;

    @NotEmpty(message = "javax.validation.constraints.NotEmpty.message")
    private String password;

    private ProfileEnum profileEnum;

    public static LoginSignupDto prepareLogin(User user) {
        return LoginSignupDto.builder()
                .email(user.getEmail())
                .password(user.getPassword())
                .profileEnum(user.getProfile())
                .build();
    }
}
