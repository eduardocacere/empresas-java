package br.com.iosys.iosysrating.dto;

import br.com.iosys.iosysrating.model.User;
import br.com.iosys.iosysrating.model.enums.ProfileEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserResponseDto {

    @Schema(name = "id", description = "Unique user identifier")
    private Long id;

    @Schema(name = "name", description = "User name identification")
    private String name;

    @Schema(name = "email", description = "User email identification")
    private String email;

    @Schema(name = "profile", description = "User profile identification")
    private ProfileEnum profile;

    public static UserResponseDto prepareUser(User user) {
        return UserResponseDto
                .builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .profile(user.getProfile())
                .build();
    }
}
