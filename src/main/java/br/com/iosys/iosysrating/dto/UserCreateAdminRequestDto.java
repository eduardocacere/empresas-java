package br.com.iosys.iosysrating.dto;

import br.com.iosys.iosysrating.model.enums.ProfileEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateAdminRequestDto extends UserCreateRequestDto {

    @NotEmpty(message = "Campo precisa ser preenchido")
    @Schema(name = "profile", description = "User profile identification")
    private ProfileEnum profile;
}
