package br.com.iosys.iosysrating.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRequestDto {

    @Schema(name = "id", description = "Unique user identifier")
    private Long id;

    @NotBlank
    @Schema(name = "name", description = "User name identification")
    private String name;

    @NotBlank
    @Schema(name = "email", description = "User email identification")
    private String email;
}
