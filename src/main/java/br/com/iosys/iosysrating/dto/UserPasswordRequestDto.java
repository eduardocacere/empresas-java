package br.com.iosys.iosysrating.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPasswordRequestDto {

    @NotNull
    @Schema(name = "id", description = "Unique user identifier")
    private Long id;

    @NotBlank
    @Schema(name = "password", description = "User password identification")
    private String password;
}
