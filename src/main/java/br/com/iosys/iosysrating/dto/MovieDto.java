package br.com.iosys.iosysrating.dto;

import br.com.iosys.iosysrating.model.Movie;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MovieDto {

    @Schema(name = "name", description = "Name movie")
    @NotEmpty(message = "javax.validation.constraints.NotEmpty.message")
    private String name;

    @Schema(name = "description", description = "Description movie")
    private String description;

    @Schema(name = "director", description = "Director movie")
    @NotEmpty(message = "javax.validation.constraints.NotEmpty.message")
    private String director;

    @Schema(name = "idGender", description = "Unique gender identifier")
    @NotNull(message = "javax.validation.constraints.NotNull.message")
    private Long idGender;

    @Schema(name = "dateRelease", description = "Date release movie")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateRelease;

    @Schema(name = "castMovies", description = "List artist of movie")
    @NotEmpty(message = "javax.validation.constraints.NotEmpty.message")
    private List<ArtistDto> castMovies;

    public static MovieDto prepareMovieDto(Movie movie) {
        return MovieDto.builder()
                .name(movie.getName())
                .description(movie.getDescription())
                .director(movie.getDirector())
                .idGender(movie.getIdGender())
                .build();
    }
}
