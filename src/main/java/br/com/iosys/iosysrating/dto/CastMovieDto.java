package br.com.iosys.iosysrating.dto;

import br.com.iosys.iosysrating.model.Artist;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CastMovieDto {

    @NotEmpty(message = "javax.validation.constraints.NotEmpty.message")
    private String name;
}
