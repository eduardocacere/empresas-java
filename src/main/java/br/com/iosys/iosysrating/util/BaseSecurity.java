package br.com.iosys.iosysrating.util;

import org.springframework.security.crypto.keygen.KeyGenerators;

public interface BaseSecurity {

    String encrypt(String textToEncrypt);
    String decrypt(String textEncrypted);
    boolean validation(String textPlan, String textEncrypted);

    default String generater(){
        return KeyGenerators.string().generateKey();
    }
}
