package br.com.iosys.iosysrating.util;

import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

public abstract class UserSecurity implements BaseSecurity {

    private static final String PASS_BASE_USER = "!Co;Z&qr30M|!KT(^yd.T)BA";
    private static final String SALT_BASE_USER = "9891c7ca16809ac1";

    private TextEncryptor encryptor = Encryptors.text(PASS_BASE_USER, SALT_BASE_USER);

    @Override
    public String encrypt(String textToEncrypt) {
        String encryptedText = encryptor.encrypt(textToEncrypt);
        return encryptedText;
    }

    @Override
    public String decrypt(String textEncrypted) {
        String encryptedText = encryptor.decrypt(textEncrypted);
        return encryptedText;
    }

    @Override
    public boolean validation(String textPlan, String textEncrypted) {
        String decryptedText = encryptor.decrypt(textEncrypted);
        return textPlan.equals(decryptedText);
    }
}
