package br.com.iosys.iosysrating.filter;

import br.com.iosys.iosysrating.exception.IosysException;
import br.com.iosys.iosysrating.service.impl.ApplicationUserDetailsService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class AuthorizationFilter extends BasicAuthenticationFilter {
    private ApplicationUserDetailsService userDetailsService;
    private String HEADER_NAME;
    private String PREFIX_HEADER;
    private String SECRET;
    private String[] PATH_ADMIN = {"/admin/**"};

    public AuthorizationFilter(AuthenticationManager authManager, ApplicationUserDetailsService userDetailsService, String secret, String headerName , String prefixName) {
        super(authManager);
        this.userDetailsService = userDetailsService;
        this.SECRET = secret;
        this.HEADER_NAME = headerName;
        this.PREFIX_HEADER = prefixName;

    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        log.info("Chamando o doFilterInternal");
        String header = request.getHeader(HEADER_NAME);

        if (header == null) {
            log.error("Requisição sem token");
            chain.doFilter(request, response);
            return;
        }
        validFormatToekn(header);

        UsernamePasswordAuthenticationToken authentication = authenticate(request);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        chain.doFilter(request, response);
    }

    private void validFormatToekn(String header) {
        if(!header.startsWith("Bearer")) {
            throw new IosysException("USR001");
        }
    }

    private UsernamePasswordAuthenticationToken authenticate(HttpServletRequest request) {
        log.info("Chamando o authenticate");
        String token = request.getHeader(HEADER_NAME).replace(PREFIX_HEADER, "");
        if (token != null) {
            Claims user = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token)
                    .getBody();

            if (user != null) {
                Map sub = user.get("sub", Map.class);
                String username = (String) sub.get("username");
                UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);

                return new UsernamePasswordAuthenticationToken(user, userDetails.getPassword(), userDetails.getAuthorities());
            }else{
                return  null;
            }

        }
        return null;
    }

//    private void validProfileAdmin(Claims user, HttpServletRequest request) {
//        String path = request.getServletPath();
//        boolean existPathAdmin = Arrays.stream(this.PATH_ADMIN).anyMatch(s -> s.contains(path));
//        if(existPathAdmin && !this.userAdmin(user)) {
//            throw new IosysException("USR001");
//        }
//    }
//
//    private boolean userAdmin(Claims user) {
//        return user.get("role").equals(ProfileEnum.ADMIN.name());
//    }
}
