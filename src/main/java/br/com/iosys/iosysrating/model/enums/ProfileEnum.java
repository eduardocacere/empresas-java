package br.com.iosys.iosysrating.model.enums;


import lombok.Getter;

public enum ProfileEnum {

    USER(0),
    ADMIN(1);

    @Getter
    private int value;

    ProfileEnum(int value) {
        this.value = value;
    }
}
