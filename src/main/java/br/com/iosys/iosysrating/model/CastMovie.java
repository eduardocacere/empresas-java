package br.com.iosys.iosysrating.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "cast_movie")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CastMovie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "id_movie")
    private Long idMovie;

    @Column(name = "id_artist")
    private Long idArtist;
}
