package br.com.iosys.iosysrating.model;

import br.com.iosys.iosysrating.model.enums.ProfileEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Optional;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;

    @Column(updatable = false)
    private String password;

    @Column(name = "date_create", updatable = false)
    private LocalDateTime dateCreate;

    @Column(name = "date_deleted", updatable = false)
    private LocalDateTime dateDeleted;
    private Boolean deleted;

    @Enumerated(EnumType.ORDINAL)
    private ProfileEnum profile;

    public static Optional<User> empty() {
        return Optional.empty();
    }

}
