package br.com.iosys.iosysrating.exception;

@SuppressWarnings("serial")
public class IosysNotFoundException extends RuntimeException  {

    public IosysNotFoundException(String message) {
        super(message);
    }

    public IosysNotFoundException() {
        super("not.found");
    }

}
