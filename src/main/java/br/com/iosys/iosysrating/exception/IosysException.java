package br.com.iosys.iosysrating.exception;

@SuppressWarnings("serial")
public class IosysException extends RuntimeException  {

    public IosysException(String message) {
        super(message);
    }

    public IosysException() {
        super("resource.not-allowed");
    }

}
