package br.com.iosys.iosysrating;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
@OpenAPIDefinition(
		info = @Info(
				title = "Iosys",
				version = "1.0",
				description = "Iosys API"
		)
)

public class IosysRatingApplication {

	public static void main(String[] args) {
		SpringApplication.run(IosysRatingApplication.class, args);
	}

}
