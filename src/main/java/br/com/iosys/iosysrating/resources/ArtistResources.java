package br.com.iosys.iosysrating.resources;

import br.com.iosys.iosysrating.dto.ArtistDto;
import br.com.iosys.iosysrating.dto.MovieDto;
import br.com.iosys.iosysrating.service.ArtistService;
import br.com.iosys.iosysrating.service.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/artist")
@RequiredArgsConstructor
public class ArtistResources {

    private final ArtistService artistService;

    @PostMapping
    @Operation(summary = "Create movie", tags = {"artist"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<ArtistDto> create(@RequestBody @Valid ArtistDto artistDto) {
        ArtistDto artistSaved = this.artistService.create(artistDto);
        return ResponseEntity.ok(artistSaved);
    }

    @GetMapping
    @Operation(summary = "List all artist", tags = {"artist"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<Page<ArtistDto>> findAll(@ParameterObject Pageable page) {
        Page<ArtistDto> artists = this.artistService.findAll(page);
        return ResponseEntity.ok(artists);
    }


}
