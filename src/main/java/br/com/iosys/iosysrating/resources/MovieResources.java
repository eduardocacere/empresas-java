package br.com.iosys.iosysrating.resources;

import br.com.iosys.iosysrating.dto.GenderDto;
import br.com.iosys.iosysrating.dto.MovieDataDto;
import br.com.iosys.iosysrating.dto.MovieDetailsDto;
import br.com.iosys.iosysrating.dto.MovieDto;
import br.com.iosys.iosysrating.dto.RatingDto;
import br.com.iosys.iosysrating.dto.UserCreateRequestDto;
import br.com.iosys.iosysrating.dto.UserRequestDto;
import br.com.iosys.iosysrating.dto.UserResponseDto;
import br.com.iosys.iosysrating.service.GenderService;
import br.com.iosys.iosysrating.service.MovieService;
import br.com.iosys.iosysrating.service.RatingService;
import br.com.iosys.iosysrating.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/movie")
@RequiredArgsConstructor
public class MovieResources {

    private final MovieService movieService;
    private final GenderService genderService;
    private final RatingService ratingService;

    @PostMapping
    @Operation(summary = "Create movie", tags = {"movie"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<MovieDto> create(@RequestBody @Valid MovieDto movie) {
        this.movieService.create(movie);
        return ResponseEntity.ok(movie);
    }

    @GetMapping("/gender")
    @Operation(summary = "List all gender", tags = {"movie"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<Page<GenderDto>> findAllGender(@ParameterObject Pageable page) {
        return ResponseEntity.ok(this.genderService.findAll(page));
    }

    @PostMapping("/rating")
    @Operation(summary = "Create rating to movie", tags = {"movie"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<RatingDto> rating(@RequestBody @Valid RatingDto rating) {
        return ResponseEntity.ok(this.ratingService.create(rating));
    }

    @GetMapping("/")
    @Operation(summary = "List all movie", tags = {"movie"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<Page<MovieDto>> findByData(@ModelAttribute MovieDataDto movieDataDto, @ParameterObject Pageable page) {

        return ResponseEntity.ok(this.movieService.findByData(movieDataDto, page));
    }

    @GetMapping("/{idMovie}")
    @Operation(summary = "List all movie", tags = {"movie"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<MovieDetailsDto> findByMovie(Long idMovie) {

        return ResponseEntity.ok(this.movieService.findDetail(idMovie));
    }




}
