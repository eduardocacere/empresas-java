package br.com.iosys.iosysrating.resources;

import br.com.iosys.iosysrating.dto.UserCreateAdminRequestDto;
import br.com.iosys.iosysrating.dto.UserCreateRequestDto;
import br.com.iosys.iosysrating.dto.UserPasswordRequestDto;
import br.com.iosys.iosysrating.dto.UserRequestDto;
import br.com.iosys.iosysrating.dto.UserResponseDto;
import br.com.iosys.iosysrating.service.UserService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminResources {

    private final UserService userService;

    @GetMapping()
    @Operation(summary = "List all users active paginated", tags = {"admin"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<Page<UserResponseDto>> find(@ParameterObject Pageable page, @RequestHeader("Authorization") String token) {
        return ResponseEntity.ok(this.userService.findAll(page));
    }

    @PostMapping
    @Operation(summary = "Create users", tags = {"admin"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<UserResponseDto> create(@RequestBody @Valid UserCreateAdminRequestDto userCreateResquest, @RequestHeader("Authorization") String token) {
        UserResponseDto userResponseDto = this.userService.createAdmin(userCreateResquest);
        return ResponseEntity.ok(userResponseDto);
    }

    @PutMapping
    @Operation(summary = "Update users", tags = {"admin"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<UserResponseDto> update(@RequestBody @Valid UserRequestDto userRequest, @RequestHeader("Authorization") String token) {
        Optional<Integer> updated = this.userService.updateData(userRequest);
        if(!updated.isPresent()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok().build();
    }

    @PutMapping("/password")
    @Operation(summary = "Upadate password of users", tags = {"admin"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<UserResponseDto> updatePassword(@RequestBody @Valid UserPasswordRequestDto userRequest, @RequestHeader("Authorization") String token) {
        Optional<Integer> updated = this.userService.updatePassword(userRequest);
        if(!updated.isPresent()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete users", tags = {"admin"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity delete(@PathVariable Long id, @RequestHeader("Authorization") String token) {
        boolean deleted = this.userService.delete(id).isPresent();
        if(!deleted) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok().build();
    }
}
