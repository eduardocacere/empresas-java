package br.com.iosys.iosysrating.resources;

import br.com.iosys.iosysrating.dto.LoginSignupDto;
import br.com.iosys.iosysrating.dto.UserCreateAdminRequestDto;
import br.com.iosys.iosysrating.dto.UserResponseDto;
import br.com.iosys.iosysrating.service.LoginService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/login/signup")
@RequiredArgsConstructor
public class LoginResources {

    private final LoginService loginService;

    @PostMapping
    @Operation(summary = "Login users", tags = {"login"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation"),
            @ApiResponse(responseCode = "204", description = "When user does not exist in the Database")
    })
    public ResponseEntity<String> login(@RequestBody @Valid LoginSignupDto loginSignup) {

        return ResponseEntity.ok(this.loginService.signup(loginSignup));
    }
}
