package br.com.iosys.iosysrating.repository;

import br.com.iosys.iosysrating.model.Movie;
import br.com.iosys.iosysrating.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    Optional<Movie> findById(Long id);

    @Query(value = "select m.*, r.rating from movie m\n" +
            "left join gender g on m.id_gender = g.id\n" +
            "left join cast_movie cm on m.id = cm.id_movie\n" +
            "left join artist a on cm.id_artist = a.id\n" +
            "left join rating r on m.id = r.id_movie\n" +
            "where ( :name is null or lower(m.name) like %:name% )\n" +
            "and ( :gender is null or m.id_gender = :gender )\n" +
            "and ( :idArtists is null or a.id in (:idArtists))", nativeQuery = true)
    Page<Movie> findByData(@Param("name") String name, @Param("gender") Long idGender, @Param("idArtists") List<Long> idArtists, Pageable pageable);
}
