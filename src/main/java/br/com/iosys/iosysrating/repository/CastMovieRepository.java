package br.com.iosys.iosysrating.repository;

import br.com.iosys.iosysrating.model.CastMovie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CastMovieRepository extends JpaRepository<CastMovie, Long> {

    List<CastMovie> findByIdMovie(Long idMovie);

}
