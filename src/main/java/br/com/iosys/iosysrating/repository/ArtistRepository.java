package br.com.iosys.iosysrating.repository;

import br.com.iosys.iosysrating.dto.ArtistDto;
import br.com.iosys.iosysrating.model.Artist;
import br.com.iosys.iosysrating.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ArtistRepository extends JpaRepository<Artist, Long> {

    Optional<Artist> findByName(String name);
    Optional<Artist> findById(Long id);

}
