package br.com.iosys.iosysrating.repository;

import br.com.iosys.iosysrating.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Page<User> findByDeleted(Pageable page, boolean deleted);
    Optional<User> findByIdAndDeleted(Long id, Boolean deleted);
    Optional<User> findByEmailAndDeleted(String email, boolean deleted);
    Optional<User> findByEmailAndPasswordAndDeleted(String email, String password, boolean deleted);

    @Transactional
    @Modifying
    @Query("update User u set u.name = :name, u.email = :email where u.id = :id")
    Optional<Integer> updateData(@Param("id") Long id, @Param("name") String name, @Param("email") String email);

    @Transactional
    @Modifying
    @Query("update User u set u.password = :password where u.id = :id")
    Optional<Integer> updatePassword(@Param("id") Long id, @Param("password") String password);

    @Transactional
    @Modifying
    @Query("update User u set u.deleted = true where u.id = :id")
    Optional<Integer> deleteUSer(@Param("id") Long id);

}
