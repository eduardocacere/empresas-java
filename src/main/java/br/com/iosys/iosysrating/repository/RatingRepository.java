package br.com.iosys.iosysrating.repository;

import br.com.iosys.iosysrating.model.CastMovie;
import br.com.iosys.iosysrating.model.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RatingRepository extends JpaRepository<Rating, Long> {


    List<Rating> findByIdMovie(Long idMovie);


}
