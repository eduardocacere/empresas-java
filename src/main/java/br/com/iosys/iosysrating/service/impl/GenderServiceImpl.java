package br.com.iosys.iosysrating.service.impl;

import br.com.iosys.iosysrating.dto.GenderDto;
import br.com.iosys.iosysrating.repository.GenderRepository;
import br.com.iosys.iosysrating.service.GenderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class GenderServiceImpl implements GenderService {

    private final GenderRepository genderRepository;

    @Override
    public Page<GenderDto> findAll(Pageable page) {
        log.info("Consultando todos os generos");
        return this.genderRepository.findAll(page).map(GenderDto::prepareGenderDto);
    }

    @Override
    public GenderDto findById(Long idGender) {
        return this.genderRepository.findById(idGender).map(GenderDto::prepareGenderDto).get();
    }
}
