package br.com.iosys.iosysrating.service.impl;

import br.com.iosys.iosysrating.dto.LoginSignupDto;
import br.com.iosys.iosysrating.dto.UserCreateAdminRequestDto;
import br.com.iosys.iosysrating.dto.UserCreateRequestDto;
import br.com.iosys.iosysrating.dto.UserPasswordRequestDto;
import br.com.iosys.iosysrating.dto.UserRequestDto;
import br.com.iosys.iosysrating.dto.UserResponseDto;
import br.com.iosys.iosysrating.exception.IosysNotFoundException;
import br.com.iosys.iosysrating.model.User;
import br.com.iosys.iosysrating.model.enums.ProfileEnum;
import br.com.iosys.iosysrating.repository.UserRepository;
import br.com.iosys.iosysrating.service.UserSecurityService;
import br.com.iosys.iosysrating.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public UserResponseDto createUser(UserCreateRequestDto userRequest) {
        return this.createUser(User.builder()
                .name(userRequest.getName())
                .email(userRequest.getEmail())
                .password(UserSecurityService.getInstance().encrypt(userRequest.getPassword()))
                .dateCreate(LocalDateTime.now())
                .profile(ProfileEnum.USER)
                .deleted(false)
                .build());
    }

    @Override
    public UserResponseDto createAdmin(UserCreateAdminRequestDto userRequest) {
        return this.createUser(User.builder()
                .name(userRequest.getName())
                .email(userRequest.getEmail())
                .password(UserSecurityService.getInstance().encrypt(userRequest.getPassword()))
                .dateCreate(LocalDateTime.now())
                .profile(userRequest.getProfile())
                .deleted(false)
                .build());
    }

    @Override
    public Page<UserResponseDto> findAll(Pageable page) {
        PageRequest pageRequest = PageRequest.of(page.getPageNumber(), page.getPageSize(), page.getSortOr(Sort.by("name").ascending()));
        return this.userRepository
                .findByDeleted(pageRequest, false)
                .map(UserResponseDto::prepareUser);
    }

    @Override
    public Optional<LoginSignupDto> findByEmail(String email) {
        return this.userRepository.findByEmailAndDeleted(email, false)
                .map(LoginSignupDto::prepareLogin);
    }

    @Override
    public Optional<Integer> updateData(UserRequestDto userRequest) {
        this.foundUser(this.userRepository.findByIdAndDeleted(userRequest.getId(), false));
        return this.userRepository.updateData(userRequest.getId(), userRequest.getName(), userRequest.getEmail());
    }

    @Override
    public Optional<Integer> updatePassword(UserPasswordRequestDto userRequest) {
        this.foundUser(this.userRepository.findByIdAndDeleted(userRequest.getId(), false));
        return this.userRepository.updatePassword(userRequest.getId(), UserSecurityService.getInstance().encrypt(userRequest.getPassword()));
    }

    @Override
    public Optional<Integer> delete(Long id) {
        this.foundUser(this.userRepository.findByIdAndDeleted(id, false));
        return this.userRepository.deleteUSer(id);
    }

    @Override
    public UserResponseDto findById(Long id) {
        Optional<User> optionalUser = this.userRepository.findByIdAndDeleted(id, false);
        if(!optionalUser.isPresent()) {
            throw new IosysNotFoundException("MOV002");
        }
        return optionalUser.map(UserResponseDto::prepareUser).get();
    }

    private void foundUser(Optional<User> optional) {
        optional.orElseThrow(() -> new IosysNotFoundException());
    }

    private UserResponseDto createUser(User userEntity) {
        return Optional.of(this.userRepository.save(userEntity))
                .map(UserResponseDto::prepareUser)
                .get();
    }
}
