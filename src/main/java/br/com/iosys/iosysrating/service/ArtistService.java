package br.com.iosys.iosysrating.service;

import br.com.iosys.iosysrating.dto.ArtistDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ArtistService {

    Optional<ArtistDto> findByName(String name);
    ArtistDto create(ArtistDto artist);
    Page<ArtistDto> findAll(Pageable pageable);
    ArtistDto findById(Long idArtis);
}
