package br.com.iosys.iosysrating.service;

import br.com.iosys.iosysrating.dto.RatingDto;

public interface RatingService {

    RatingDto create(RatingDto ratingDto);
    Integer mediaVoting(Long idMovie);
}
