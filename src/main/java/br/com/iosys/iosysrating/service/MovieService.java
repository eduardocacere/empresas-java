package br.com.iosys.iosysrating.service;

import br.com.iosys.iosysrating.dto.MovieDataDto;
import br.com.iosys.iosysrating.dto.MovieDetailsDto;
import br.com.iosys.iosysrating.dto.MovieDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MovieService {

    MovieDto create(MovieDto movie);
    MovieDto findMovie(Long id);
    Page<MovieDto> findByData(MovieDataDto movieDataDto, Pageable page);
    MovieDetailsDto findDetail(Long idMovie);
}
