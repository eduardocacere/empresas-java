package br.com.iosys.iosysrating.service;

import br.com.iosys.iosysrating.dto.ArtistDto;

import java.util.List;

public interface CastMovieService {

    void create(ArtistDto artist, Long idMovie);

    List<ArtistDto> findByCastMovie(Long idMovie);
}
