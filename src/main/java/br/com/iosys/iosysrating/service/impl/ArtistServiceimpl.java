package br.com.iosys.iosysrating.service.impl;

import br.com.iosys.iosysrating.dto.ArtistDto;
import br.com.iosys.iosysrating.dto.UserResponseDto;
import br.com.iosys.iosysrating.model.Artist;
import br.com.iosys.iosysrating.repository.ArtistRepository;
import br.com.iosys.iosysrating.service.ArtistService;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class ArtistServiceimpl implements ArtistService {

    private final ArtistRepository artistRepository;

    @Override
    public Optional<ArtistDto> findByName(String name) {
        return this.artistRepository.findByName(name)
                .map(ArtistDto::prepareArtist);
    }

    @Override
    public ArtistDto create(ArtistDto artist) {
        return ArtistDto.prepareArtist(this.artistRepository.save(Artist.builder().name(artist.getName()).build()));
    }

    @Override
    public Page<ArtistDto> findAll(Pageable page) {
        PageRequest pageRequest = PageRequest.of(page.getPageNumber(), page.getPageSize(), page.getSortOr(Sort.by("name").ascending()));
        return this.artistRepository
                .findAll(pageRequest)
                .map(ArtistDto::prepareArtist);
    }

    @Override
    public ArtistDto findById(Long idArtis) {
        return this.artistRepository.findById(idArtis).map(ArtistDto::prepareArtist).get();
    }
}
