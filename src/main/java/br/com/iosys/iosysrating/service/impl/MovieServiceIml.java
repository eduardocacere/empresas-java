package br.com.iosys.iosysrating.service.impl;

import br.com.iosys.iosysrating.dto.ArtistDto;
import br.com.iosys.iosysrating.dto.GenderDto;
import br.com.iosys.iosysrating.dto.MovieDataDto;
import br.com.iosys.iosysrating.dto.MovieDetailsDto;
import br.com.iosys.iosysrating.dto.MovieDto;
import br.com.iosys.iosysrating.exception.IosysNotFoundException;
import br.com.iosys.iosysrating.model.Movie;
import br.com.iosys.iosysrating.repository.MovieRepository;
import br.com.iosys.iosysrating.service.CastMovieService;
import br.com.iosys.iosysrating.service.GenderService;
import br.com.iosys.iosysrating.service.MovieService;
import br.com.iosys.iosysrating.service.RatingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class MovieServiceIml implements MovieService {

    private final MovieRepository movieRepository;
    private final CastMovieService castMovieService;
    private final GenderService genderService;
    private final RatingService ratingService;

    @Override
    public MovieDto create(MovieDto movie) {
        Movie movieToSave = Movie.builder()
                .name(movie.getName())
                .description(movie.getDescription())
                .idGender(movie.getIdGender())
                .dateRelease(movie.getDateRelease())
                .build();

        this.validArtisExist(movie);

        Optional.of(this.movieRepository.save(movieToSave))
        .ifPresent(m -> saveCastMovie(movie.getCastMovies(), m.getId()));
        return movie;
    }

    @Override
    public MovieDto findMovie(Long id) {
        Optional<Movie> optionalMovie = this.movieRepository.findById(id);
        if(!optionalMovie.isPresent()) {
            throw new IosysNotFoundException("MOV002");
        }
        return optionalMovie.map(MovieDto::prepareMovieDto).get();
    }

    @Override
    public Page<MovieDto> findByData(MovieDataDto movieData, Pageable page) {
        log.info("Consultando dados do filme {}",  movieData);
        PageRequest pageRequest = PageRequest.of(page.getPageNumber(), page.getPageSize(), page.getSortOr((Sort.by("name")).and(Sort.by("r.rating").descending())));
        List<Long> idArtists = movieData.getIdArtists().isEmpty() ? null : movieData.getIdArtists();
        return this.movieRepository.findByData(movieData.getNameMovie().orElse(null).toLowerCase(),
                movieData.getIdGender().orElse(null),
                idArtists,
                pageRequest)
                .map(MovieDto::prepareMovieDto);

    }

    @Override
    public MovieDetailsDto findDetail(Long idMovie) {
        log.info("Consultando dados do filme detalhado do filme {}", idMovie);
        MovieDto movieDto = this.findMovie(idMovie);
        GenderDto genderDto = this.genderService.findById(movieDto.getIdGender());
        List<ArtistDto> artists = this.castMovieService.findByCastMovie(idMovie);

        MovieDetailsDto movieDetails = MovieDetailsDto.prepareDetails(movieDto);
        movieDetails.setGender(genderDto);
        movieDetails.setCastMovies(artists);
        movieDetails.setVotingMedia(this.ratingService.mediaVoting(idMovie));
        return movieDetails;
    }

    private void saveCastMovie(List<ArtistDto> artists, Long id) {
        artists.forEach(a -> this.castMovieService.create(a, id));
    }

    private void validArtisExist(MovieDto movie) {
        boolean allArtistExist = movie.getCastMovies()
                .stream().anyMatch(a -> Optional.ofNullable(a.getId()).isPresent());

        if(!allArtistExist) {
            throw new IosysNotFoundException("MOV001");
        }
    }
}
