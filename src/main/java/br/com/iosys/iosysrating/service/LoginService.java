package br.com.iosys.iosysrating.service;

import br.com.iosys.iosysrating.dto.LoginSignupDto;

public interface LoginService {

    String signup(LoginSignupDto loginSignup);
}
