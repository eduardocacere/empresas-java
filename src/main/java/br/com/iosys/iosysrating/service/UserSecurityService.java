package br.com.iosys.iosysrating.service;

import br.com.iosys.iosysrating.util.UserSecurity;
import lombok.Builder;

@Builder
public class UserSecurityService extends UserSecurity {

    public static UserSecurityService getInstance(){
        return UserSecurityService.builder().build();
    }
}
