package br.com.iosys.iosysrating.service.impl;

import br.com.iosys.iosysrating.dto.MovieDto;
import br.com.iosys.iosysrating.dto.RatingDto;
import br.com.iosys.iosysrating.model.Rating;
import br.com.iosys.iosysrating.repository.RatingRepository;
import br.com.iosys.iosysrating.service.MovieService;
import br.com.iosys.iosysrating.service.RatingService;
import br.com.iosys.iosysrating.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class RatingSeriveImpl implements RatingService {

    private final RatingRepository ratingRepository;

    @Override
    public RatingDto create(RatingDto ratingDto) {
        log.info("Avaliando Filme {}", ratingDto);

        Rating ratingToSave = Rating.builder()
                .rating(ratingDto.getRating())
                .idMovie(ratingDto.getIdMovie())
                .idUser(ratingDto.getIdUser())
                .dateCreate(LocalDateTime.now())
                .build();
        Rating saved = this.ratingRepository.save(ratingToSave);
        return RatingDto.prepareRatingDto(saved);
    }

    @Override
    public Integer mediaVoting(Long idMovie) {
        log.info("Calculando a media de votos para o filme {}", idMovie);
        List<Rating> ratingList = this.ratingRepository.findByIdMovie(idMovie);
        if(ratingList.isEmpty()) {
            return 0;
        }
        int size = ratingList.size();
        int totalValue = ratingList.stream().mapToInt(v -> v.getRating())
                .sum();
        return  Math.floorDiv(totalValue, size);
    }

}
