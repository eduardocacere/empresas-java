package br.com.iosys.iosysrating.service;

import br.com.iosys.iosysrating.dto.LoginSignupDto;
import br.com.iosys.iosysrating.dto.UserCreateAdminRequestDto;
import br.com.iosys.iosysrating.dto.UserCreateRequestDto;
import br.com.iosys.iosysrating.dto.UserPasswordRequestDto;
import br.com.iosys.iosysrating.dto.UserRequestDto;
import br.com.iosys.iosysrating.dto.UserResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface UserService {

    UserResponseDto createAdmin(UserCreateAdminRequestDto userRequest);
    UserResponseDto createUser(UserCreateRequestDto userRequest);
    Page<UserResponseDto> findAll(Pageable page);
    UserResponseDto findById(Long id);
    Optional<LoginSignupDto> findByEmail(String email);
    Optional<Integer> updateData(UserRequestDto userRequest);
    Optional<Integer> updatePassword(UserPasswordRequestDto userRequest);
    Optional<Integer> delete(Long id);
}
