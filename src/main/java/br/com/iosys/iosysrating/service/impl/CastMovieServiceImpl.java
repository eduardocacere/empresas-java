package br.com.iosys.iosysrating.service.impl;

import br.com.iosys.iosysrating.dto.ArtistDto;
import br.com.iosys.iosysrating.model.CastMovie;
import br.com.iosys.iosysrating.repository.CastMovieRepository;
import br.com.iosys.iosysrating.service.ArtistService;
import br.com.iosys.iosysrating.service.CastMovieService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class CastMovieServiceImpl implements CastMovieService {

    private final CastMovieRepository castMovieRepository;
    private final ArtistService artistService;

    @Override
    public void create(ArtistDto artist, Long idMovie) {
        log.info("Criando o Associacao de Elenco {} para o filme {}", artist, idMovie);
        this.castMovieRepository.save(this.prepare(artist, idMovie));
    }

    @Override
    public List<ArtistDto> findByCastMovie(Long idMovie) {
        List<CastMovie> castMovies = this.castMovieRepository.findByIdMovie(idMovie);
        return castMovies.stream()
                .map(cm -> this.artistService.findById(cm.getIdArtist()))
                .collect(Collectors.toList());
    }

    private CastMovie prepare(ArtistDto artist, Long idMovie) {
        return CastMovie.builder()
                .idMovie(idMovie)
                .idArtist(artist.getId())
                .build();
    }
}
