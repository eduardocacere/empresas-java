package br.com.iosys.iosysrating.service.impl;

import br.com.iosys.iosysrating.dto.LoginSignupDto;
import br.com.iosys.iosysrating.model.User;
import br.com.iosys.iosysrating.repository.UserRepository;
import br.com.iosys.iosysrating.service.LoginService;
import br.com.iosys.iosysrating.service.UserSecurityService;
import br.com.iosys.iosysrating.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final ApplicationUserDetailsService userDetailsService;
    static final String CLAIM_KEY_USERNAME = "sub";
    static final String CLAIM_KEY_ROLE = "role";
    static final String CLAIM_KEY_CREATED = "created";

    @Value("${iosys.authentication.expiration}")
    private Long EXPIRATION;

    @Value("${iosys.authentication.secret}")
    private String SECRET;

    @Value("${iosys.authentication.prefix}")
    private String PREFIX;

    @Override
    public String signup(LoginSignupDto loginSignup) {
        log.info("Realizado o login do usuario");

        UserDetails userDetails = userDetailsService.loadUserByUsername(loginSignup.getEmail());

        if(!Optional.ofNullable(userDetails).isPresent()) {
            log.error("Usuario não localizado");
            throw new UsernameNotFoundException("Usuario ou senha invalida");
        }
        String decrypt = UserSecurityService.getInstance().decrypt(userDetails.getPassword());

        if(!loginSignup.getPassword().trim().equals(decrypt.trim())) {
            log.error("Dados preenchido para login nao confere");
            throw new UsernameNotFoundException("Usuario ou senha invalida");
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginSignup.getEmail(), userDetails.getPassword(), userDetails.getAuthorities()));
        SecurityContextHolder.getContext().setAuthentication(authentication);


        String token = getToken(userDetails);

        return PREFIX + " " + token;
    }

    private String getToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME, userDetails);
        userDetails.getAuthorities().forEach(authority -> claims.put(CLAIM_KEY_ROLE, authority.getAuthority()));
        claims.put(CLAIM_KEY_CREATED, new Date());

        return generateToken(claims);
    }

    private String generateToken(Map<String, Object> claims) {
        return Jwts.builder().setClaims(claims).setExpiration(generateDateExpiration())
                .signWith(SignatureAlgorithm.HS512, SECRET).compact();
    }

    private Date generateDateExpiration() {
        return new Date(System.currentTimeMillis() + EXPIRATION * 1000);
    }

}
