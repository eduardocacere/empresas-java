package br.com.iosys.iosysrating.service.impl;

import br.com.iosys.iosysrating.dto.LoginSignupDto;
import br.com.iosys.iosysrating.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Collections.emptyList;

@Service
@RequiredArgsConstructor
public class ApplicationUserDetailsService implements UserDetailsService {

    private final UserService userService;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<LoginSignupDto> optinalLogin = this.userService.findByEmail(email);
        if (!optinalLogin.isPresent()) {
            throw new UsernameNotFoundException(email);
        }

        LoginSignupDto loginSignupDto = optinalLogin.get();
        List<GrantedAuthority> authorized = new ArrayList<>();
        authorized.add(new SimpleGrantedAuthority(loginSignupDto.getProfileEnum().toString()));
        return new User(loginSignupDto.getEmail(), loginSignupDto.getPassword(), authorized);
    }
}
