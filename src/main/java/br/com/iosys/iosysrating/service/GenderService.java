package br.com.iosys.iosysrating.service;

import br.com.iosys.iosysrating.dto.GenderDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenderService {

    Page<GenderDto> findAll(Pageable page);
    GenderDto findById(Long idGender);
}
