create table if not exists movie
(
    id bigint auto_increment comment 'Identificador serial unico para tabela',
    name varchar(100) not null comment 'Identificador do nome do filme',
    description varchar(300) null comment 'Identificador da descriçao do filme',
    director varchar(80) null comment 'Identificador do nome do director do filme',
    id_gender bigint not null comment 'Identificador do genero do filme',
    date_release date not null comment 'Identificador da data de lancamento do filme',
    constraint movie_pk
        primary key (id),
    constraint m_g_id_fk
        foreign key (id_gender) references gender (id)
)
    comment 'Identifica os filmes';

