create table if not exists artist
(
    id bigint auto_increment comment 'Identificador serial unico para tabela',
    name varchar(200) not null comment 'Identificador do nome do artista',
    constraint artist_pk
        primary key (id)
);

