create table if not exists cast_movie
(
    id bigint auto_increment comment 'Identificador serial unico para tabela',
    id_movie bigint not null comment 'Identificador do filme',
    id_artist bigint not null comment 'Identificador do artista',
    constraint cast_movie_pk
        primary key (id),
    constraint cm_m_id_fk
        foreign key (id_movie) references movie (id),
    constraint cm_a_id_fk
        foreign key (id_artist) references artist (id)
)
    comment 'Identifica o elenco que participa do filme';

