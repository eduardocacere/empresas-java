create table if not exists rating
(
    id bigint auto_increment comment 'Identificador serial unico para tabela',
    rating char not null comment 'Identificador da classificacao do filme',
    id_movie bigint not null comment 'Identificador do filme que sera avaliado',
    id_user bigint not null comment 'Identificador do usuario que sera avaliado',
    date_create timestamp null comment 'Identificador da data que foi realizado a avaliacao',
    constraint rating_pk
        primary key (id),
    constraint rating_movie_id_fk
        foreign key (id_movie) references movie (id),
    constraint rating_user_id_fk
        foreign key (id_user) references user (id)
)
    comment 'Identifica a avalicao do filme por usuario';

