create table if not exists user
(
    id bigint auto_increment comment 'Identificador serial unico para tabela',
    name varchar(150) not null comment 'Nome do usuario',
    email varchar(100) not null comment 'Email do usuario',
    password varchar(200) not null comment 'Senha do usuario',
    date_create timestamp null comment 'Data de criaçao do usuario',
    deleted boolean default false not null comment 'Identificador se registro esta deletado',
    date_deleted timestamp null comment 'Data em que registro foi deletado',
    profile int default 0 not null comment 'Perfil do usuario 0=USER, 1=ADMIN',
    constraint user_pk
        primary key (id)
);

create unique index user_email_uindex
	on user (email);

create index user_id_index
	on user (id);

