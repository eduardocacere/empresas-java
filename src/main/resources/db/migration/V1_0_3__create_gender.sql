create table if not exists gender
(
    id bigint auto_increment comment 'Identificador serial unico para tabela',
    name varchar(200) not null comment 'Identificador do nome do genero',
    constraint gender_pk
        primary key (id)
);
