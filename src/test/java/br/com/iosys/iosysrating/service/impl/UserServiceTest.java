package br.com.iosys.iosysrating.service.impl;

import br.com.iosys.iosysrating.dto.UserResponseDto;
import br.com.iosys.iosysrating.exception.IosysNotFoundException;
import br.com.iosys.iosysrating.model.User;
import br.com.iosys.iosysrating.model.enums.ProfileEnum;
import br.com.iosys.iosysrating.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    private UserRepository userRepository;
    private UserServiceImpl userService;

    @BeforeEach
    void setUp() {
        userRepository = mock(UserRepository.class);
        this.userService = new UserServiceImpl(userRepository);

    }

    @Test
    public void findByIdTest() {
        User user = User.builder()
                .id(0l)
                .name("")
                .email("")
                .profile(ProfileEnum.USER)
                .build();
        when(this.userRepository.findByIdAndDeleted(any(), any())).thenReturn(Optional.of(user));
        UserResponseDto userResponse = this.userService.findById(1l);
        assertTrue(Optional.ofNullable(userResponse).isPresent());

    }

    @Test
    public void findByIdWithExecptionTest() {
        User user = User.builder()
                .id(0l)
                .name("")
                .email("")
                .profile(ProfileEnum.USER)
                .build();
        when(this.userRepository.findByIdAndDeleted(any(), any())).thenReturn(Optional.empty());
        assertThrows(IosysNotFoundException.class, () -> this.userService.findById(1l));

    }
}
