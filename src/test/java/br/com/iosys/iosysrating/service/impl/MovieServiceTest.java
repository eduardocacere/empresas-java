package br.com.iosys.iosysrating.service.impl;

import br.com.iosys.iosysrating.dto.ArtistDto;
import br.com.iosys.iosysrating.dto.MovieDto;
import br.com.iosys.iosysrating.exception.IosysNotFoundException;
import br.com.iosys.iosysrating.model.Movie;
import br.com.iosys.iosysrating.repository.MovieRepository;
import br.com.iosys.iosysrating.service.CastMovieService;
import br.com.iosys.iosysrating.service.GenderService;
import br.com.iosys.iosysrating.service.RatingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static java.util.Arrays.asList;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatCode;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MovieServiceTest {

    private MovieRepository movieRepository;
    private CastMovieService castMovieService;
    private GenderService genderService;
    private RatingService ratingService;
    private MovieServiceIml movieService;

    @BeforeEach
    void setUp() {
        movieRepository = mock(MovieRepository.class);
        castMovieService = mock(CastMovieService.class);
        genderService = mock(GenderService.class);
        ratingService = mock(RatingService.class);
        movieService = new MovieServiceIml(movieRepository, castMovieService, genderService, ratingService);

    }

    @Test
    public void createTest() {
        MovieDto movieDto = MovieDto.builder()
                .name("")
                .description("")
                .idGender(1l)
                .castMovies(asList(ArtistDto.builder().id(1l).build()))
                .dateRelease(LocalDate.now()).build();
        when(this.movieRepository.save(any())).thenReturn(Movie.builder().build());

        this.movieService.create(movieDto);
        assertThatCode(() -> this.movieRepository.save(any()));
    }

    @Test
    public void createWithExecptionTest() {
        MovieDto movieDto = MovieDto.builder()
                .name("")
                .description("")
                .idGender(1l)
                .castMovies(asList())
                .dateRelease(LocalDate.now()).build();
        when(this.movieRepository.save(any())).thenReturn(Movie.builder().build());
        assertThrows(IosysNotFoundException.class, () -> this.movieService.create(movieDto));
    }
}
