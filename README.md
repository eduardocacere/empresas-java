# O que é esse serviço?

O Iosys-Rating é responsável pelo gerenciamento de filmes e votação.

# Como funciona?

O iosys-rating, irá disponibilizar endpoints para gestão de filmes.

# Como executar localmente

## Requisitos:

1. JDK 8
2. Docker

## Passo a passo

1. Suba a infraestrutura local executando: `docker-compose -f docker-compose-local.yml up -d`. Será executado Mysql.
2. Confira se os containers estão executando: `docker ps -a`
3. Execute o projeto em sua IDE ou via linha de comando: `mvn spring-boot:run`
4. Após executar os migration será criado as tabelas e massa de dados.
5. Para o acesso como administrador pode ser utilizado o email: iosys@iosys.com.br com senha: 123456
6. Após executar o projeto acessar o endereço: http://localhost:8080/swagger-ui.html

OBS: Todos os usuários cadastrados utilizam a senha: 123456


### Dependencias:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.0/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.0/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.5.0/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.5.0/reference/htmlsingle/#boot-features-jpa-and-spring-data)
* [Flyway Migration](https://docs.spring.io/spring-boot/docs/2.5.0/reference/htmlsingle/#howto-execute-flyway-database-migrations-on-startup)
